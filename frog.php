<?php

require_once('animal.php');
class Frog extends Animal {
    
    public function __construct($string) {
        $this->name = $string;
    }

    public function jump() {
        echo "Jump: hop hop<br><br>";
    }
}

?>