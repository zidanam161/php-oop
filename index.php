<?php

require_once('animal.php');
require('ape.php');
require('frog.php');

$shaun = new Animal("shaun");
echo "Name: " . $shaun->name . "<br>";
echo "Legs: " . $shaun->legs . "<br>";
echo "Cold Bloded: " . $shaun->cold_bloded . "<br><br>";

$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold Bloded: " . $kodok->cold_bloded . "<br>";
$kodok->jump();

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold Bloded: " . $sungokong->cold_bloded . "<br>";
$sungokong->yell();

?>